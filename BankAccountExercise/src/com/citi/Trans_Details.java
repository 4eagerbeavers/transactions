package com.citi;


public class Trans_Details {

	private double amt;
	
	
	public Trans_Details(double amt, String type) {
	
		this.amt = amt;
		this.type = type;
	}

	public double getAmt() {
		return amt;
	}
	
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	private String type;
	
	
	}

