package com.citi;

import java.util.ArrayList;

	public class BankAccountCreation
	{
	//data fields
		private int id;
		private String accountHolder;
		private double balance;
		private String tranHistory="Transaction History: \n";
		private String accnt_History="Number of Accounts: \n";
		//private Date creationTimestamp;
		private static int nextid=1;
		
	private ArrayList<Trans_Details> transactions= new ArrayList<Trans_Details>();
		
		public static int getNextid()   //should be placed at the end
		{
			return nextid;
		}

	//Default Constructor		
		public BankAccountCreation()
		{
			this("NA"); //invoking the alternate constructor
		}
	//Constructors	
		public BankAccountCreation(String accountHolder)
		{
			this.id=++nextid;
		    this.accountHolder=accountHolder;
		}
		
		
	//getter, setters
		public double getBalance() {
			return balance;
		}

		public void setBalance(double balance) {
			this.balance = balance;
		}
		
		public void addHistory(String transaction){
			this.tranHistory=this.tranHistory.concat("\n"+transaction);
		}
//		public void addHistoryAcc(String accnts){
//			this.accnt_History=this.accnt_History.concat("\n"+accnts);
//		}
		
		public int getId()
		{
			return this.id;
		}
		
		public void setId(int id)
		{
			this.id=id;
		}
		public String getAccounHolder()
		{
			return this.accountHolder;
		}
		public void setAccountHolder(String accountHolder)
		{
		  this.accountHolder=accountHolder;	
		}
		
		public String withdraw(double amount)
		{
			if(this.balance-amount>=0)
			{
				this.balance-=amount;
				//transactions.add(new Trans_Details(amount,"Withdraw"));
				addHistory("Withdrawal Successful! Amount: "+ amount);
				return "Current Balance : " + this.balance;	
			}
			else
			{
				return "Insufficient Balance!! \n Current Balance : " + this.balance;
			}
			
		}
		public String deposit(double amount)
		{
			this.balance+=amount;
			//transactions.add(new Trans_Details(amount,"Deposit"));
			addHistory("Deposit Successful! Amount: "+ amount);
			return "Current Balance : " + this.balance;
		}

		@Override
		public String toString()
		{
			String header = String.format("[%d] %s %.2f", this.id, this.accountHolder, this.balance);

		return header + '\n' + this.tranHistory;
			
		}



	}

	

